# Trabalho Individual 02 2021.1

**Nome**: Wagner Martins da Cunha

**Matrícula**: 18/0029177

## Sobre

O objetivo deste trabalho é configurar a integração contínua do projeto presente neste respositório, utilizando o gitlab CI. 

A configuração é feita a partir do arquivo [gitlab-ci.yml](./.gitlab-ci.yml) e foi utilizado o serviço Docker in Docker (dind). 

A integração ocorre em três estágios:
build, test e deploy.

No estágio de build, são criadas as imagens docker do projeto e salvas no [_register_](https://gitlab.com/wagnermc506/workshop-ci-entrega-02/container_registry) do projeto no gitlab para uso nas etapas seguintes.

No estágio test, as aplicações são testadas, e se tudo passar, segue-se a etapa de deploy.

No estágio de deploy, as imagens são enviadas para o register do docker:

- Frontend: https://hub.docker.com/r/wagnermc506/workshop-ci-entrega-02_frontend
- Backend: https://hub.docker.com/r/wagnermc506/workshop-ci-entrega-02_backend

